<?php

/**
 * This file contains settings for qterest;
 */

if (!defined('ABSPATH')) {
    exit;
}

global $qterest_settings;

$qterest_settings = array(
    'search' => false,
    'contact' => true,
    'mailchimp' => true
);
